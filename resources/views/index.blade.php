<style>
    .dropdown-toggle::after{
        border-top:0!important;
        border-left:0!important;
        border-right:0!important;
        border-bottom:0!important;
    }
</style>
@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div id="bg-oranges" class="col-12 p-0 position-relative text-center py-5">
                <h1 class="judul">CV. Cendana Perkasa</h1>
                <span class="d-block text-white">
                    Produsen Produk Aren dan Turunannya 1. Gula Aren Cetak 2. Gula Tualah 3. Nira Aren 
                    <br> 
                    Akan buka pukul 09.00 besok
                </span>
                <br>
                <button class="button-animate">Whatsapp</button>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <section class="py-5 text-center">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
                <div class="item">
                    <div class="card w-100">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                    </div>
                </div>
            </div>
        </section>
        <section class="py-4 text-center">
            <h1>Lorem Ipsum</h1>
            <span>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </span>
        </section>
        <section class="mt-5 py-5">
            <div class="row">
                <div class="col-lg-6">
                    <img class="w-100" src="{{ asset('images/dekstop.jpg')}}" alt="" srcset="">
                </div>
                <div class="col-lg-6">
                    <h4>Lorem Ipsum</h4>
                    <span>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </span>
                </div>
            </div>
        </section>
        <section class="mt-5 py-5">
            <div class="row">
                <div class="col-lg-12 text-center mb-5 py-3"><span class="d-relative thumnail-judul">Testimoni</span></div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="col-5 pl-0 text-left">
                                    <span>3 bulan lalu</span>
                                </div>
                                <div class="col-3">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i style="font-size:20px" class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <blockquote>
                                        "Terima kasih gula aren medan, rasa nya enak, dan pekingannya rapi Rekomen buat teman teman untuk pesan di sini.."
                                    </blockquote>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="nameQuote">- Aditya</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="col-5 pl-0 text-left">
                                    <span>3 bulan lalu</span>
                                </div>
                                <div class="col-3">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i style="font-size:20px" class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <blockquote>
                                        "Terima kasih gula aren medan, rasa nya enak, dan pekingannya rapi Rekomen buat teman teman untuk pesan di sini.."
                                    </blockquote>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="nameQuote">- Aditya</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="col-5 pl-0 text-left">
                                    <span>3 bulan lalu</span>
                                </div>
                                <div class="col-3">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i style="font-size:20px" class="fa fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-12">
                                    <blockquote>
                                        "Terima kasih gula aren medan, rasa nya enak, dan pekingannya rapi Rekomen buat teman teman untuk pesan di sini.."
                                    </blockquote>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="nameQuote">- Aditya</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-5">
                    <div class="row">
                        <div class="col-lg-6 mb-5 text-center text-lg-right"><a class="nameQuote" href=""><b>Tulisan Ulasa</b></a></div>
                        <div class="col-lg-6 mb-5 text-center text-lg-left pl-lg-0"><a class="nameQuote" href=""><b>Baca Selengkapnya</b></a></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="mt-3 p-5 mb-5" style="background-color:#f6f6f6">
            <div class="row p-5">
                <div class="col-lg-12 text-center mb-5 py-3"><span class="d-relative thumnail-judul">Informasi Selengkapnya</span></div>
                <div class="col-lg-12 mb-4">
                    <b>UMKM Arendu Arenta</b>
                    <br>
                    <span>Alamat Produksi : Dusun 2 Desa Rumah Sumbul Kecamatan Sibolangit</span>
                </div>
                <div class="col-lg-12">
                    <span>Kantor Pemasaran : Jln. Bunga Rinte Gg. Mawar Sharon 2 Medan</span>
                    <br>
                    <span>https://wa.me/c/6285772754429</span>
                </div>
            </div>
        </section>
        <section class="mt-5">
            <div class="row">
                <div class="col-lg-12 text-center mb-5 py-3"><span class="d-relative thumnail-judul">Hubungi Kami</span></div>
                <div class="col-12 mb-5">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.2745360150825!2d98.6158449147584!3d3.5239046974354085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30312561a6eb24eb%3A0x1c429d8c1f4897aa!2sJUAL%20GULA%20AREN%20MEDAN%20(UMKM%20Arendu%20Arenta)!5e0!3m2!1sen!2sid!4v1612695529107!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-12 mb-3"><b>Kontak</b></div>
                                <div class="col-12 mb-3">
                                    <button class="button-animate2">Telephone</button>
                                </div>
                                <div class="col-12"><span>0857-7275-4429</span></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-12 mb-3"><b>Alamat</b></div>
                                <div class="col-12 mb-3">
                                    <button class="button-animate2">Lihat Rute</button>
                                </div>
                                <div class="col-12">
                                    <span>
                                        Jl. Bunga Rinte Gg. Mawar Sharon 2 Kel. 
                                        <br>
                                        Simpang Selayang Kec. Medan
                                        <br>
                                        Tuntungan
                                        <br>
                                        Medan
                                        <br>
                                        Sumatera Utara 20135
                                        <br>
                                        Indonesia
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-12 mb-3"><b>Jam Buka</b></div>
                                <div class="col-12">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Sen: </td>
                                                <td>09.00–17.00</td>
                                            </tr>
                                            <tr>
                                                <td>Sel: </td>
                                                <td>09.00–17.00</td>
                                            </tr>
                                            <tr>
                                                <td>Rab: </td>
                                                <td>09.00–17.00</td>
                                            </tr>
                                            <tr>
                                                <td>Kam: </td>
                                                <td>09.00–17.00</td>
                                            </tr>
                                            <tr>
                                                <td>Jum: </td>
                                                <td>09.00–17.00</td>
                                            </tr>
                                            <tr>
                                                <td>Sab: </td>
                                                <td>09.00–17.00</td>
                                            </tr>
                                            <tr>
                                                <td>Min:</td>
                                                <td>Tutup</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

