@extends('layouts.master')

@section('content')
    <div class="container mt-5">
        <section class="py-5 text-center">
            <h1>Our Product</h1>
            <span>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </span>
        </section>
        <section class="mt-3">
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <div class="input-group input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">
                                <i class="fa fa-search" style="font-size:24px"></i>
                            </span>
                        </div>
                        <input placeholder="Search" type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                    </div>
                </div>
            </div>
        </section>
        <section  class="mt-4">
            <div class="row">
                <div class="col-lg-4 mb-4">
                    <div class="card w-100">
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="text-orange"><b>Pesan</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="card w-100">
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="text-orange"><b>Pesan</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="card w-100">
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="text-orange"><b>Pesan</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="card w-100">
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="text-orange"><b>Pesan</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="card w-100">
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="text-orange"><b>Pesan</b></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-4">
                    <div class="card w-100">
                        <img class="card-img-top" src="{{ asset('images/mac.jpg')}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title text-orange">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="text-orange"><b>Pesan</b></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

