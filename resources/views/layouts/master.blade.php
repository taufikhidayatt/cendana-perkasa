<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/hamburgers.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">   
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Palanquin+Dark&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/owl.carousel.js') }}"></script>
</head>
<body>
    <header class="border-bottom position-fixed w-100">
        <div class="container-fluid px-5">
            <div class="row mx-lg-5">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-3">
                            <div class="containers" onclick="sidenav(this)">
                                <div class="bar1"></div>
                                <div class="bar2"></div>
                                <div class="bar3"></div>
                            </div>
                        </div>
                        <div class="col-lg-11 col-md-11 col-9 pl-0">
                            <a class="nav-link d-none d-lg-block d-xl-block d-md-block judul" href="#"><h4>CV. Cendana Perkasa</h4></a>
                            <a class="nav-link d-lg-none d-xl-none d-md-none judul mt-2" href="#"><h5>CV. Cendana Perkasa</h6></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="mt-5">
        @include('layouts.sidebar')

        @yield('content')
    </main>
    <footer class="mt-auto">
        <div class="container">
            <section class="mt-5 border-top py-4">
                <div class="row">
                    <div class="col-lg-6 text-center mb-2 text-lg-left">
                        <a href="" style="font-size:11px;color:#b5b5b5">Laporkan penyalahgunaan</a>
                    </div>
                    <div class="col-lg-6 text-center mb-2 text-lg-right"></div>
                </div>
            </section>
        </div>
        <!-- <div class="container px-0 py-3 fixed-footer position-fixed w-100">
            <div class="row">
                <div class="col-4"><a class="nav-link" href="">WhatsApp</a></div>
                <div class="col-4"><a class="nav-link" href="">Telephone</a></div>
                <div class="col-4"><a class="nav-link" href="">Lihat Rute</a></div>
            </div>
        </div> -->
    </footer>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay:true,
            autoplayTimeout:5000,
            responsive: {
                0: {
                    items: 1,
                    nav: true,
                    loop:true
                },
                600: {
                    items: 1,
                    nav: false,
                    loop:true
                },
                1000: {
                    items: 1,
                    nav: true,
                    loop: true,
                    margin: 20
                }
            }
        })
    })

    function sidenav(x) {
        var sidenav = document.getElementById("mySidenav").offsetWidth;

        if(sidenav == 250){
            document.getElementById("mySidenav").style.width = "0";
        } else{
            document.getElementById("mySidenav").style.width = "250px";
        }

        x.classList.toggle("change");
    }
</script>
</html>